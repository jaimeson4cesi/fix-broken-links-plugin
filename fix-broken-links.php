<?php

declare( strict_types = 1 );
namespace FixBrokenLinks
{
	/*
	Plugin Name:  Fix Broken Links
	Plugin URI:   https://www.jaimeson-waugh.com
	Description:  Allows one to check and automatically Fix Broken Links in posts.
	Version:      1.0
	Author:       Jaimeson Waugh
	Author URI:   https://www.jaimeson-waugh.com
	License:      GPL2
	License URI:  https://www.gnu.org/licenses/gpl-2.0.html
	Text Domain:  fix-broken-links
	Domain Path:  /languages

	"Fix Broken Links" is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	any later version.

	"Fix Broken Links" is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with "Fix Broken Links". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
	*/

	require_once( 'fix-elements.php' );
	class FixBrokenLinks extends FixElements
	{
		//
		//  PUBLIC
		//
		/////////////////////////////////////////////////////////

			public function __construct()
			{
				parent::__construct( 'Broken Links', 'broken-links', 'a' );
			}

			protected function CheckForBrokenElement( $image ) : ?array
			{
				$url = $image->getAttribute( 'href' );
				$status_code = null;
				if ( $url )
				{
					if ( $this->TestElementIsBroken( $url ) )
					{
						return [ 'src' => $url, 'dom' => $image ];
					}
				}
				return null;
			}

			protected function ChangeElement( $element ) : void
			{
			}
	}
	new FixBrokenLinks();

}
